﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9936715
{
    public class Customer
    {
        public string Name { get; set; }
        public string Phone { get; set; }

        // Constructor to create customer
        public Customer()
        {
            Create(this);
        }

        // Create customer
        public static Customer Create(Customer customer)
        {
            Console.WriteLine("\nEnter customer details");

            // Get details
            GetDetails(customer);

            return customer;
        }

        // Update customer details
        public static Customer Update(Customer customer)
        {
            Console.Clear();
            Program.Heading("Update Customer Details");
            Console.WriteLine($"\nUpdate your details");
            
            // Get details
            GetDetails(customer);

            return customer;
        }

        // Confirm customer details
        public static Customer Confirm(Customer customer)
        {
            string select;
            Console.Clear();

            // Display current customer details
            Program.Heading("Customer Details");
            Console.WriteLine($"\nName: {customer.Name}\nPhone: {customer.Phone}\n");

            // Ask to confirm
            do
            {
                Console.Write("Confirm your details? (y/n) ");
                select = Console.ReadLine();

                if (select.StartsWith("n"))
                {
                    Update(customer);
                    break;
                }
            } while (!select.StartsWith("y"));

            return customer;
        }

        // Reusable get details method
        private static Customer GetDetails(Customer customer)
        {
            int placehold = 0;
            bool check = true;
            string phone = "";

            // Prompt for name
            Console.Write("\nName: ");
            customer.Name = Console.ReadLine();
            
            // Prompt for phone number
            do
            {
                Console.Write("Phone: ");
                phone = Console.ReadLine();
                check = int.TryParse(phone, out placehold);
            } while (!check);
            customer.Phone = phone;

            return customer;
        }
    }
}
