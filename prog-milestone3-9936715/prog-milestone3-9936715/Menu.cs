﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9936715
{
    public class Menu
    {
        // Pizza sizes
        public static List<Tuple<int, string, double>> Size = new List<Tuple<int, string, double>>()
        {
            new Tuple<int, string, double>(1, "Small", 5),
            new Tuple<int, string, double>(2, "Medium", 10),
            new Tuple<int, string, double>(3, "Large", 15)
        };


        //public static Dictionary<string, double> _Size = new Dictionary<string, double>()
        //{
        //    {"Small", 5 },
        //    {"Medium", 10 },
        //    {"Large", 15 }
        //};

        // Drink price
        public const double Drink = 3;
        
        // Pizza flavours
        public static string[] Pizza =
        {
            "Cheese",
            "Hawaiian",
            "Pepperoni",
            "Vegetarian",
            "Meat Lovers"
            };

        // Drink flavours
        public static string[] Drinks =
        {
            "Coke",
            "Lemonade",
            "Orange Juice",
            "Water"
        };
    }


}
