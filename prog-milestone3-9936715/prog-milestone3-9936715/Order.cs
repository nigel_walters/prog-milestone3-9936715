﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9936715
{
    public class Order
    {
        // Order properties
        public Customer Customer { get; set; }
        public List<OrderItem> Items { get; set; }
        public double Total { get; set; }
        public Payment Payment { get; set; }

        // Constructor logic
        public Order()
        {
            // Create customer and assign to order
            Customer = new Customer();

            // Instantiate list of order items
            Items = new List<OrderItem>();
            
            // Add pizza items to order
            Items.Add(Item(this, Menu.Pizza));
            
            // Prompt for drink
            PromptDrink(this);
            
            // Confirm details
            Customer.Confirm(this.Customer);

            // Display order
            Display(this);

            // Make payment
            Payment = new Payment(this);
        }

        // Methods

        // Add order item to order
        public static OrderItem Item(Order myOrder, string[] array, bool pizza = true)
        {
            // Initialise variables
            OrderItem orderItem = new OrderItem();
            string[] item = array;
            bool check;
            string select;
            int index = 0;

            Array.Sort(item);

            Console.Clear();
            Program.Heading(($"{(pizza ? "Pizza" : "Drinks")} Menu"));
            Console.WriteLine();

            // Display drink price
            if(!pizza)
            {
                Console.WriteLine($"Drinks are {Menu.Drink.ToString("C")} each");
                orderItem.Cost = Menu.Drink;
                Console.WriteLine();
            }

            // Choose a flavour pizza or drink
            Program.Heading($"Flavour", false);
            for (int i = 0; i < item.Length; i++)
            {
                Console.WriteLine($"{i + 1}. {item[i]}");
            }
            Console.WriteLine();
            do
            {
                Console.Write("Select flavour: ");
                check = int.TryParse(Console.ReadLine(), out index);
                index--;
            } while (!check || index >= item.Length || index < 0);

            // Assign item flavour
            orderItem.Item = item[index];

            // Prompt for pizza size
            if (pizza)
            {
                Console.WriteLine();
                Program.Heading($"Pizza Size", false);
                // Iterate through tuple list
                foreach (var x in Menu.Size)
                {
                    Console.WriteLine($"{x.Item1}. {x.Item2} - {x.Item3.ToString("C")}");
                }
                Console.WriteLine();
                do
                {
                    Console.Write("Select size: ");
                    check = int.TryParse(Console.ReadLine().ToUpper(), out index);
                } while (!check || index > Menu.Size.Count || index < 0);
                // Assign size and cost of item
                foreach (var x in Menu.Size)
                {
                    if (x.Item1 == index)
                    {
                        orderItem.Size = x.Item2;
                        orderItem.Cost = x.Item3;
                        break;
                    }
                }
            }

            // Prompt for quantity
            Console.WriteLine();
            do
            {
                Console.Write("Enter quantity: ");
                check = int.TryParse(Console.ReadLine(), out index);
            } while (!check || index < 0);

            // Assign quantity to item
            orderItem.Quantity = index;

            // Calculate and assign item price
            orderItem.Price = orderItem.Cost * orderItem.Quantity;

            // Check if they would like more
            Console.WriteLine();
            do
            {
                Console.Write($"Would you like more {(pizza ? "pizza" : "drinks")}? (y/n) ");
                select = Console.ReadLine();

                // Add another item to order
                if (select.StartsWith("y"))
                {
                    if (pizza)
                    {
                        // Pizza item
                        myOrder.Items.Add(Item(myOrder, Menu.Pizza));
                    }
                    else
                    {
                        // Drink item
                        myOrder.Items.Add(Item(myOrder, Menu.Drinks, false));
                    }
                    break;
                }
            } while (!select.StartsWith("n"));

            return orderItem;
        }

        // Prompt for drink 
        public static void PromptDrink(Order order)
        {
            string select;

            Console.WriteLine();

            do
            {
                Console.Write("Would you like a drink? (y/n) ");
                select = Console.ReadLine();
                
                if (select.StartsWith("y"))
                {
                    // Add drink item to order
                    order.Items.Add(Item(order, Menu.Drinks, false));
                    break;
                }
            } while (!select.StartsWith("n"));

            return;
        }

        // Display order
        public static void Display(Order order)
        {
            double total = 0;

            Console.Clear();
            Program.Heading("Order Details");
            Console.WriteLine();

            // Iterate through order items and display
            foreach (var item in order.Items)
            {
                Console.WriteLine($"{item.Item}{(item.Size==null? "": $" - {item.Size}")}\n{item.Quantity} @ {item.Cost.ToString("C")} each - {item.Price.ToString("C")}\n");
                total += item.Price;
            }

            // Assign order total and display
            order.Total = total;
            Program.Heading($"Total: {total.ToString("C")}");

            // Pause screen
            Console.WriteLine("\nPress Enter to continue...");
            Console.ReadLine();

            return;
        }
    }
}
