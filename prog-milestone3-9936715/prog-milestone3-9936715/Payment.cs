﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9936715
{
    public class Payment
    {
        // Running balance
        public double Balance { get; set; }

        public Payment(Order order)
        {
            //Prompt for money
            Pay(this, order);
        }

        // Payment method
        private static void Pay(Payment payment, Order order, bool initial = true)
        {
            bool check = true;
            double amount = 0;

            if (initial)
            {
                payment.Balance = order.Total;
                Console.Clear();
                Program.Heading("Order Payment");
            }

            Console.WriteLine();

            // Display balance owing
            Console.WriteLine($"{(initial?"Order Total":"Remaining Balance")}: {payment.Balance.ToString("C")}");
            
            //Prompt for payment
            do
            {
                Console.Write("Enter payment: ");
                check = double.TryParse(Console.ReadLine(), out amount);
            } while (!check);

            // Subtract amount from balance
            payment.Balance -= amount;

            // Check if balance is outstanding
            if(payment.Balance > 0)
            {
                // Re-prompt for further payment - recursive
                Pay(payment, order, false);
            }
            else
            {
                // Thank you message displaying change
                Console.WriteLine($"\nThank you {order.Customer.Name}!\nChange: {Math.Abs(payment.Balance).ToString("C")}\n");
            }

        }

        private static void Remaining(Payment payment, Order order)
        {

        }
    }
}
