﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9936715
{
    class Program
    {
        // Main entry into program
        static void Main(string[] args)
        {
            // App heading on first page
            Heading("Pizza Order");

            // Create order
            Order MyOrder = new Order();
        }

        // Methods

        // Heading method
        public static void Heading(string heading, bool main = true)
        {
            if (main)
            {
                Underline(heading);
            }
            Console.WriteLine(heading);
            Underline(heading);
        }

        // Over/Underline method - decorative for heading
        private static void Underline(string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
            return;
        }

    }
}
